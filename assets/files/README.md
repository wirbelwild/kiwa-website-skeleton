# Files 

Store all your binary data here. 

If you don't need this folder feel free to remove it. If you want to use that folder, get sure to add a command to copy the folder when creating the build files in your `webpack.config.js`.