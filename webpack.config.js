const Encore = require("@symfony/webpack-encore");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const isProduction = Encore.isProduction();

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || "dev");
}

Encore
    .setOutputPath("public/build/")
    .setPublicPath("/build")
    .addEntry("template", "./assets/components/template.js")
    .enableBuildNotifications()
    .enableSourceMaps(!isProduction)
    .enableVersioning(isProduction)
    .enableSassLoader()
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .addPlugin(new CopyWebpackPlugin({
        patterns: [
            {
                from: "./assets/images",
                to: "images"
            },
        ]
    }))
;

if (isProduction) {
    Encore.configureFilenames({
        js: "[name].[contenthash:8].js",
        css: "[name].[contenthash:8].css"
    });
}

module.exports = Encore.getWebpackConfig();