# Config 

Store all your config data inside this folder.

The `config.php` is your main configuration and contains a minimum basic configuration to start with when creating a new project. The easiest way to configure the website is by using the Kiwa Console. This will write the config by its own. 

The Kiwa Console can be found under `/bin/console` and should come along with this project. 

Is it missing? Added it to your project by running `$ composer require kiwa/console`.

To modify the config by your own, check the [ConfigGenerator.php](https://bitbucket.org/wirbelwild/kiwa-core/src/master/src/Config/Generator/ConfigGenerator.php) and the [PageGenerator.php](https://bitbucket.org/wirbelwild/kiwa-core/src/master/src/Config/Generator/PageGenerator.php) class. They're making it also very easy to create the config as they both provide a lot of readable getter methods.
