<?php

/**
 * Kiwa. A feather-light web framework for small but professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @link https://www.kiwa.io
 * @license MIT
 *
 * This file has been auto-generated at 2020-06-12 20:11:42.
 * It allows an easier handling of the config file. Alternatively the config file may also contain a pure php array
 * or an array stored as YAML file.
 * @kiwa-auto-generated
 */

use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;

$configGenerator = new ConfigGenerator();
$configGenerator
    /**
     * Definitions for the URL, the pages and the language.
     */
    /** Sets the URL where this page will run. */
    ->setMainURL('https://www.kiwa.io')
    
    /** Sets the suffix for all pages. */
    ->setPageSuffix('html')

    /** Disallows to link pages of different languages. */
    ->enableStrictLanguageHandling()

    /** Sets the URL structure. */
    ->setURLStructure('name', 'subname')

    /** Forces all URLs to have `www`. */
    ->enableWWW()

    /** Forces all URLs to have `https`. */
    ->enableSSL()
    
    /**
     * Sets a page. 
     * This one defines the index page.
     */
    ->addPage(PageGenerator::create()
        /** Sets the language for this page. */
        ->setLanguageCode('en')
        
        /** Sets the name of this page. This will be used for the URL. */
        ->setName('index')
        
        /** Sets the name of the `phtml` file which should be used. */
        ->setFile('index')
        
        /** Sets the page title. */
        ->setPageTitle('Welcome to Kiwa')
        
        /** Sets the page description. */
        ->setDescription('This is your first page.')
    )
    
    /**
     * At least here are some information to reach a good SEO.
     */
    
    /** Sets a default og:title. */
    ->setDefaultOGTitle('en', 'Your site\'s default title')

    /** Sets a default description. */
    ->setDefaultDescription('en', 'Your site\'s default description')

    /** Sets the default viewport. */
    ->setDefaultViewport('width=device-width, initial-scale=1')

    /** Sets the default crawling setting. */
    ->setDefaultRobots('index, follow')
;

return $configGenerator->getConfig();