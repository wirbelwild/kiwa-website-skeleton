# Public 

This folder contains all public files. Your domain should point to this folder. 

Here may be stored for example:

-   The `robots.txt` file. The Kiwa Console can create that automatically.
-   The `sitemap.xml` file. Kiwa Console can that also create for you.
-   A `build` folder with all your assets in it.