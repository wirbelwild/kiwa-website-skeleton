<?php

/**
 * Kiwa. A feather-light web framework for small but professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

use Kiwa\Frontend\Controller;

$autoload = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

if (!file_exists($autoload)) {
    echo 'Unable to find "vendor/autoload.php".' . PHP_EOL;
    echo 'Maybe you should try to install your dependencies by running "$ composer install".' . PHP_EOL;
    exit(1);
}

require_once $autoload;

ini_set('session.cookie_httponly', 1);

$controller = new Controller();
$controller->printResponse();
