#!/usr/bin/env php
<?php

/**
 * Kiwa. A feather-light web framework for small but professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

use BitAndBlack\Composer\Composer;
use Kiwa\TemplateConsole\Environment;

$vendor = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'vendor';
$autoloadFile = $vendor . DIRECTORY_SEPARATOR . 'autoload.php';
$consoleFile = $vendor . DIRECTORY_SEPARATOR . 'kiwa' . DIRECTORY_SEPARATOR . 'console' . DIRECTORY_SEPARATOR . 'kiwa-console.php';

if (!file_exists($autoloadFile)) {
    echo 'Unable to find "' . $autoloadFile . '".' . PHP_EOL;
    echo 'Maybe you need to install composers dependencies at first: "$ composer install".' . PHP_EOL;
    exit(0);
}

if (!file_exists($consoleFile)) {
    echo 'Unable to find "' . $consoleFile . '".' . PHP_EOL;
    echo 'Maybe you should try to reinstall the console module: "$ composer require kiwa/console".' . PHP_EOL;
    exit(0);
}

require $autoloadFile;

if (Composer::classExists(Environment::class)) {
    Environment::runsFromTemplate();
}

require $consoleFile;
