[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/website-skeleton)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/kiwa/website-skeleton/v/stable)](https://packagist.org/packages/kiwa/website-skeleton)
[![Total Downloads](https://poser.pugx.org/kiwa/website-skeleton/downloads)](https://packagist.org/packages/kiwa/website-skeleton)
[![License](https://poser.pugx.org/kiwa/website-skeleton/license)](https://packagist.org/packages/kiwa/website-skeleton)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa Website Skeleton 

The skeleton for creating a website with Kiwa.

## Requirements

This skeleton is made for the use with [Composer](https://packagist.org/packages/kiwa/website-skeleton). Be sure to get it at first. 

## Usage 

### Installation 

1.  Use your CLI and Composer to create a new website by running `composer create-project kiwa/website-skeleton my-project`. Change `my-project` by your needs.

2.  Move to your project directory, for example by running `cd my-project`.

3.  Create your code repository, for example by running `git init`.

    By running the installation a lot of basic files will be created, for example an `assets` folder for all your asset files. Those files can be committed to your repository.

4.  Install all dependencies by running `composer install` and `yarn install`. If you're using NPM instead of Yarn or don't have those scripts installed globally, those commands may differ in your system.

### Creating the website

Everything you want to do can be done with the help of the Kiwa Console, which is our own command line tool and that is stored in the `bin` folder. It will help creating pages and more. Run `php bin/console` to get more information.

### Running the website 

To access the website from public, point your domain to the `public` folder so the `index.php` inside will be used. 

To run the website in a local environment, run `php bin/console server:start` and open the URL that it tells in your browser. This will be mostly [http://127.0.0.1:8000](http://127.0.0.1:8000). 

In case you don't want to use the Kiwa Console, you can also run a local PHP server by your own: `php -S 127.0.0.1:8000 public/index.php`.

## Documentation

The documentation about how to use Kiwa can be found under [www.kiwa.io/documentation.html](https://www.kiwa.io/documentation.html).

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).