<?php

namespace App\Tests;

use Generator;
use Kiwa\Config\Page;
use Kiwa\DI;
use Kiwa\Frontend\Controller;
use Kiwa\Page\PageList;
use Kiwa\URL\URLFromPage;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\Attributes\RunInSeparateProcess;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * A basic smoke test.
 *
 * @package App\Tests
 */
class SmokeTest extends TestCase
{
    /**
     * Pages with a different response code.
     *
     * @var array<string, int>
     */
    private array $responseCodePages = [
        //'login' => Response::HTTP_MOVED_PERMANENTLY,
    ];

    /**
     * Provides all defined pages.
     *
     * @return Generator<array{
     *     url: string,
     *     page: Page,
     * }>
     */
    public static function getAllPages(): Generator
    {
        $controller = new Controller();
        $pageList = new PageList();

        foreach ($pageList as $key => $page) {
            $dataProviderKey = $key . '-' . $page->getName();

            yield $dataProviderKey => [
                'url' => (string) new URLFromPage($page),
                'page' => $page,
            ];
        }

        unset($controller);
    }

    /**
     * Tests if a page can be loaded correctly.
     *
     * @param string $url
     * @param Page $page
     * @return void
     */
    #[RunInSeparateProcess]
    #[DataProvider('getAllPages')]
    public function testCanAccessAllPages(string $url, Page $page): void
    {
        $_SERVER['REQUEST_URI'] = $url;

        $controller = new Controller();

        $serverStatusExpected = Response::HTTP_OK;

        if (array_key_exists($page->getName(), $this->responseCodePages)) {
            $serverStatusExpected = $this->responseCodePages[$page->getName()];
        }

        self::assertSame(
            $serverStatusExpected,
            DI::getResponse()->getStatusCode()
        );

        $response = DI::getResponse()->getContent();

        self::assertIsString($response);

        if (Response::HTTP_OK === $serverStatusExpected) {
            self::assertStringContainsString(
                'page-' . $page->getFile(),
                $response,
                $url
            );
        }

        unset($controller);
    }
}
